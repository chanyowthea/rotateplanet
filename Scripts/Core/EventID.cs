﻿public enum EventID
{
    Hello, 
    // player
    RotatePlanet, 
    AttackFromPlanet, 
    CreateTurret,
    CreateTurretSuccess,
    End,
    AddHealth,

    // view
    AddGold, 
    UpdateGold,
    AddScore,
    UpdateScore,
}
